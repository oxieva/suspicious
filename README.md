# Suspicious readings #

App to detect suspicious readings

## Description

This is a symfony console command called **holaluz: suspicious-readings** that returns suspicious readings to us.

We have different types of files with electricity readings and we we will try to identify readings that are either 
higher or lower than the annual median ± 50%.


## Usage
Save the document to read in the folder 

```bash
var/readings
```
Open the terminal and write the command

```bash
bin/console holaluz:suspicious-readings
```

Get the suspicious readings

## Installation

Clone the project: 
cd /var/www/symfony

```bash
cd my_project
git clone git@bitbucket.org:oxieva/suspicious.git
```

Go to the root 
For example: cd /var/www/symfony/suspicious
```bash
cd my_project
git clone git@bitbucket.org:oxieva/suspicious.git
composer install

```
Write the holaluz command:
eva@eva-vant:/var/www/symfony/suspicious$ php bin/console holaluz:suspicious-readings

```bash
bin/console holaluz:suspicious-readings

```
Get the output
```bash
+---------------+---------+------------+---------+
| Client        | Month   | Suspicious | Median  |


```

## Installation from virtual estructure 
We have different containers: webserver, mysql, phpmyadmin,redis and redisadmin

We have the code in a submodule: 
www
suspicious
 [
102d671eed5d
]


```bash
cd my_project
git clone git@bitbucket.org:halion33/sf_docker_holaluz.git
```

If you are using Linux
For example: cd /var/www/docker/sf_docker_holaluz
```bash
Run the script
sh ./holaluz_up.sh 
```
This is the script:
sudo chown www-data:www-data ./www -R && sudo docker-compose exec -uwww-data webserver bash -c "cd /var/www/html && composer install" && sudo docker-compose exec -uwww-data webserver bash -c "cd /var/www/htm$



```
Get the output
```bash
+---------------+---------+------------+---------+
| Client        | Month   | Suspicious | Median  |


```


## Repository
[Bitbucket](https://bitbucket.org/oxieva/suspicious/src/master/)

## Contact if you have a problem
eva.lujan@gmail.com
