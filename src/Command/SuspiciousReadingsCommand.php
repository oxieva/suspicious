<?php

namespace App\Command;

use App\Factory\ReadingsParserFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File;
use App\Service\ReadingsDetector\SuspiciousDetector;
use Symfony\Component\Console\Helper\Table;


class SuspiciousReadingsCommand extends Command
{
    protected static $defaultName = 'holaluz:suspicious-readings';

    protected  $readingsFolder;

    protected $parserFactory;

    protected $suspiciousDetector;

    public function __construct($readingsFolder, ReadingsParserFactory $parserFactory, SuspiciousDetector $suspiciousDetector)
    {
        parent::__construct();
        $this->readingsFolder = $readingsFolder;
        $this->parserFactory = $parserFactory;
        $this->suspiciousDetector = $suspiciousDetector;
    }

    protected function configure()
    {
       $this
        ->setDescription('Holaluz --- Finds suspicious readings')

        ->setHelp('This command allows you to find suspicious readings on a file...')
    ;
    }


    //@todo Create SuspiciousDetector service and inject
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $files = array_diff(scandir($this->readingsFolder), array('..', '.'));

        $data = [];

        // Any type of file
        foreach($files as $file){
            $fileObject = new File($this->readingsFolder . '/' . $file);
            $parser = $this->parserFactory->createParser($fileObject);
            $data = array_merge($data, $parser->parse($fileObject));
        }


        //Getting suspicious service
        $customerLimits = $this->suspiciousDetector->calculateCustomersLimits($data);

        $suspiciousReadings = $this->suspiciousDetector->detectSuspiciousReadings($customerLimits,$data);


        if(count($suspiciousReadings) > 0 ){

            $table = new Table($output);
            $table
                ->setHeaders(['Client', 'Month', 'Suspicious', 'Median']);

            $rows = [];
            foreach($suspiciousReadings as $reading) {
                $rows[] = [$reading['Client'], $reading['Month'], $reading['Suspicious'], $reading['Median']];
            }

            $table->setRows($rows);


            $table->render();



        }else{
            $output->writeln([
                'No suspicious readings! Wow!'
            ]);
        }


    }

}
