<?php
namespace App\Factory;

use Symfony\Component\HttpFoundation\File\File;
use App\Service\ReadingsParser\CSVReadingsParser;
use App\Service\ReadingsParser\XMLReadingsParser;
class ReadingsParserFactory{

    public function createParser(File $file)
    {
        $parser = null;
        $extension = $file->getExtension();
        switch ($extension) {
            case 'csv':
                $parser = new CSVReadingsParser();
                break;
            case 'xml':
                $parser = new XMLReadingsParser();
                break;
            default:
                return false;
                break;
        }

        // ...

        return $parser;
    }


}