<?php

namespace App\Service\ReadingsDetector;

class SuspiciousDetector{

    public function calculateCustomersLimits($parsedData){

        $clientsMedian = [];

        foreach($parsedData as $clientId => $clientData){

            asort($clientData);

            $dataValues = array_values($clientData);

            $readings5 = $dataValues[5];
            $readings6 = $dataValues[6];

            $median= ($readings5 + $readings6) / 2 ;

            //50% increase

            $clientsMedian[$clientId]= $median;



            $percentSuspicious = 50;

            $percentInDecimal = $percentSuspicious / 100;


            $percent = $percentInDecimal * $median;


            $clientsLimits[$clientId]['median'] = $median;

            $clientsLimits[$clientId]['min'] = round($median - $percent,1);
            $clientsLimits[$clientId]['max'] = round($median + $percent,1);


        }

       return $clientsLimits;


    }

    public function detectSuspiciousReadings($clientsLimits, $parsedData){
        $suspiciousReadings = [];

        foreach ($parsedData as $clientId => $clientData ){

            $min = $clientsLimits[$clientId]['min'];
            $max = $clientsLimits[$clientId]['max'];
            $median = $clientsLimits[$clientId]['median'];


            foreach ($clientData as $period => $reading){

                if($reading < $min || $reading > $max){

                    //Create array with the results
                    $suspiciousReadings[] = [
                        'Client' => $clientId,
                        'Month' => $period,
                        'Suspicious' => $reading,
                        'Median' => $median
                    ];

                }
            }
        }

        return $suspiciousReadings;
    }
}