<?php

namespace App\Service\ReadingsParser;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\File;

class CSVReadingsParser{

    public function parse(File $file){

        $csv = array_map('str_getcsv', file($file));
        $parsedData = [];

        //For remove header column
        /*array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv);*/

        foreach($csv as $reading){

            if(isset($reading[1]) && isset($reading[2])) {
                if((int)$reading[1] != 0) {

                    //[ClientID][period] = [reading]
                    $parsedData[$reading[0]][$reading[1]] = $reading[2];
                }
            }else{
                throw new Exception('Inconsistent data. Check the file and try again');
            }

        }

        /*dump($parsedData);
       exit();*/
        return $parsedData;



        //return ['client1' => ['mes','lectura']];
    }

}