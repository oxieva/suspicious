<?php

namespace App\Service\ReadingsParser;

use Symfony\Component\HttpFoundation\File\File;
use XMLReader;
use SimpleXMLElement;

class XMLReadingsParser{

    public function parse(File $file){

        $reader = new XMLReader();

        if (!$reader->open($file)) {
            die('Failed to open file');
        }

        $parsedData = [];
        //$parsedData[$reading[0]][$reading[1]] = $reading[2];


        while($reader->read()) {
            if($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'reading' ) {


                $reading = new SimpleXMLElement($reader->readOuterXml());
                $readingValue = (string) $reading;
                //var_dump($reading); exit();
                $attributes = $reading->attributes();
                //[ClientID][period] = [reading]
                $parsedData[(string)$attributes->clientID][(string) $attributes->period] = $readingValue;

            }


        }
        $reader->close();

        //dump($parsedData); exit();
        return $parsedData;

    }

}